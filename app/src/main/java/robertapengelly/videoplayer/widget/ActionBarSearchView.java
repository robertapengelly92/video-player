package robertapengelly.videoplayer.widget;

import  android.app.SearchableInfo;
import  android.content.Context;
import  android.content.res.Resources;
import  android.graphics.drawable.GradientDrawable;
import  android.support.v7.view.CollapsibleActionView;
import  android.support.v7.widget.SearchView;
import  android.util.AttributeSet;
import  android.util.DisplayMetrics;
import  android.util.TypedValue;
import  android.view.Gravity;
import  android.view.View;
import  android.view.ViewGroup;

public class ActionBarSearchView extends SearchView implements CollapsibleActionView {

    private final SearchAutoComplete mSearchSrcTextView;
    
    public ActionBarSearchView (Context context) {
        this (context, null);
    }
    
    public ActionBarSearchView (Context context, AttributeSet attrs) {
        this (context, attrs, android.support.v7.appcompat.R.attr.searchViewStyle);
    }
    
    public ActionBarSearchView (Context context, AttributeSet attrs, int defStyleAttr) {
        super (context, attrs, defStyleAttr);
        
        this.mSearchSrcTextView = findViewById (android.support.v7.appcompat.R.id.search_src_text);
        this.mSearchSrcTextView.setGravity (Gravity.CENTER_VERTICAL);
        this.mSearchSrcTextView.setTextSize (TypedValue.COMPLEX_UNIT_SP, 14);
    
    }
    
    @Override
    public void onActionViewExpanded () {
        super.onActionViewExpanded ();
        
        int height = 0xFFFFFFFE;
        
        Resources res = getContext ().getResources ();
        DisplayMetrics dm = res.getDisplayMetrics ();
        View parent = (View) getParent ();
        
        if (parent != null) {
            height = (int) (parent.getMeasuredHeight () * 0.75f);
        }
        
        ViewGroup.LayoutParams lp = getLayoutParams ();
        lp.height = height;
        lp.width = 0xFFFFFFFF;
        
        if (lp instanceof ViewGroup.MarginLayoutParams) {
        
            ((ViewGroup.MarginLayoutParams) lp).rightMargin = (int) (dm.density * 20);
            ((ViewGroup.MarginLayoutParams) lp).setMarginEnd ((int) (dm.density * 20));
        
        }
        
        setLayoutParams (lp);
    
    }
    
    @Override
    protected void onSizeChanged (int w, int h, int oldw, int oldh) {
        super.onSizeChanged (w, h, oldw, oldh);
        
        GradientDrawable gd = (GradientDrawable) getBackground ();
        
        if (gd == null) {
            return;
        }
        
        float radii = Math.min (w, h) * 0.5f;
        
        gd.setCornerRadii (new float[] { radii, radii, radii, radii, radii, radii, radii, radii });
        setBackground (gd);
    
    }
    
    @Override
    public void setSearchableInfo (SearchableInfo searchable) {
        super.setSearchableInfo (searchable);
        
        this.mSearchSrcTextView.setPrivateImeOptions ("");
    
    }

}