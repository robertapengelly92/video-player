package robertapengelly.videoplayer.view;

import  android.app.Activity;
import  android.view.Menu;
import  android.view.MenuItem;

public class MenuItemToggle implements MenuItem.OnActionExpandListener {

    private final Activity activity;
    private final Menu menu;
    
    public MenuItemToggle (Activity activity, Menu menu) {
    
        this.activity = activity;
        this.menu = menu;
    
    }
    
    @Override
    public boolean onMenuItemActionCollapse (MenuItem item) {
    
        activity.invalidateOptionsMenu ();
        return false;
    
    }
    
    @Override
    public boolean onMenuItemActionExpand (MenuItem item) {
    
        MenuItem mi;
        
        for (int i = 0; i < menu.size (); i++) {
        
            mi = menu.getItem (i);
            
            if (mi != item) {
                mi.setVisible (false);
            }
        
        }
        
        return true;
    
    }

}