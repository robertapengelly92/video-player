package robertapengelly.videoplayer;

import  android.app.SearchManager;
import  android.content.Context;
import  android.content.Intent;
import  android.content.SharedPreferences;
import  android.content.res.ColorStateList;
import  android.content.res.Resources;
import  android.content.res.TypedArray;
import  android.graphics.Color;
import  android.graphics.drawable.Drawable;
import  android.graphics.drawable.GradientDrawable;
import  android.os.Bundle;
import  android.support.v7.app.ActionBar;
import  android.support.v7.app.AppCompatActivity;
import  android.support.v7.widget.SearchView;
import  android.support.v7.widget.Toolbar;
import  android.util.DisplayMetrics;
import  android.view.Menu;
import  android.view.MenuItem;
import  android.view.View;
import  android.widget.Button;

import  robertapengelly.videoplayer.app.SortDialogFragment;
import  robertapengelly.videoplayer.view.MenuItemToggle;

public class MainActivity extends AppCompatActivity implements
    SortDialogFragment.OnDismissListener {
    
    private boolean action_bar_custom_showing = false;
    private boolean dark_mode = false;
    private boolean descending = true;
    private boolean is_searching = false;
    private boolean show_dialog_fragment_sort = false;
    
    private String search_query = "";
    private String sort_by = "date_modified";
    private String view_type = "grid";
    
    private ActionBar action_bar;
    private Button btn_delete;
    private Button btn_share;
    private SearchView menu_search_view;
    private SharedPreferences prefs;
    private SortDialogFragment sort_dialog_fragment;
    private Toolbar toolbar;
    
    private Drawable createSearchViewBackground () {
    
        GradientDrawable d = new GradientDrawable();
        d.setColor (Color.parseColor (this.dark_mode ? "#424242" : "#F5F5F5"));
        d.setShape (GradientDrawable.RECTANGLE);
        
        ColorStateList colorControlNormal;
        TypedArray ta = getTheme ().obtainStyledAttributes (new int[] { R.attr.colorControlNormal });
        
        try {
            colorControlNormal = ta.getColorStateList (0);
        } finally {
            ta.recycle ();
        }
        
        DisplayMetrics dm = getResources ().getDisplayMetrics ();
        d.setStroke ((int) (dm.density * 1), colorControlNormal);
        
        return d;
    
    }
    
    private void loadSettings () {
    
        if (this.prefs == null) {
            this.prefs = getSharedPreferences (getPackageName (), 0);
        }
        
        dark_mode = this.prefs.getBoolean ("DARK_MODE", false);
        descending = this.prefs.getBoolean ("DESCENDING", true);
        
        sort_by = this.prefs.getString ("SORT_BY", "date_modified");
        view_type = this.prefs.getString ("VIEW_TYPE", "grid");
    
    }
    
    @Override
    public void onBackPressed () {
    
        if (this.action_bar_custom_showing) {
        
            //noinspection ConstantConditions
            this.action_bar_custom_showing = !this.action_bar_custom_showing;
            
            this.action_bar.setDisplayShowCustomEnabled (this.action_bar_custom_showing);
            this.action_bar.setDisplayShowHomeEnabled (!this.action_bar_custom_showing);
            this.action_bar.setDisplayShowTitleEnabled (!this.action_bar_custom_showing);
            
            invalidateOptionsMenu ();
        
        } else {
            super.onBackPressed ();
        }
    
    }
    
    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        
        loadSettings ();
        
        if (this.dark_mode) {
            setTheme (R.style.AppTheme);
        } else {
            setTheme (R.style.AppTheme_Light);
        }
        
        this.sort_dialog_fragment = new SortDialogFragment ();
        this.sort_dialog_fragment.addOnDismissListener (this);
        
        if (savedInstanceState != null) {
        
            this.action_bar_custom_showing = (savedInstanceState.getInt ("STATE_CUSTOM_SHOWING", 0) != 0);
            this.is_searching = (savedInstanceState.getInt ("STATE_SEARCHING", 0) != 0);
            this.search_query = savedInstanceState.getString ("STATE_QUERY", "");
        
        }
        
        setContentView (R.layout.activity_main);
        
        this.toolbar = findViewById (R.id.action_bar);
        setSupportActionBar (this.toolbar);
        
        if (getSupportActionBar () != null) {
        
            this.action_bar = getSupportActionBar ();
            
            View v = getLayoutInflater ().inflate (R.layout.action_bar_select_view, this.toolbar, false);
            
            this.btn_delete = v.findViewById (R.id.action_bar_btn_delete);
            this.btn_delete.setVisibility (View.GONE);
            
            this.btn_share = v.findViewById (R.id.action_bar_btn_share);
            this.btn_share.setVisibility (View.GONE);
            
            this.action_bar.setCustomView (v);
            
            this.action_bar.setDisplayShowCustomEnabled (this.action_bar_custom_showing);
            this.action_bar.setDisplayShowHomeEnabled (!this.action_bar_custom_showing);
            this.action_bar.setDisplayShowTitleEnabled (!this.action_bar_custom_showing);
        
        }
    
    }
    
    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
    
        if (this.action_bar_custom_showing) {
            return false;
        }
        
        getMenuInflater ().inflate (R.menu.activity_main, menu);
        
        ColorStateList colorControlNormal;
        Resources.Theme theme = this.toolbar.getContext ().getTheme ();
        TypedArray ta = theme.obtainStyledAttributes (new int[] { R.attr.colorControlNormal });
        
        try {
            colorControlNormal = ta.getColorStateList (0);
        } finally {
            ta.recycle ();
        }
        
        Drawable icon;
        MenuItem item;
        View view;
        
        int size = menu.size ();
        SearchManager sm = (SearchManager) getSystemService (Context.SEARCH_SERVICE);
        
        for (int i = 0; i < size; i++) {
        
            item = menu.getItem (i);
            view = item.getActionView ();
            
            if (item.getItemId () == R.id.menu_search) {
            
                item.setOnActionExpandListener (new MenuItemToggle (this, menu));
                
                if (view instanceof SearchView) {
                
                    this.menu_search_view = (SearchView) view;
                    
                    this.menu_search_view.setBackground (createSearchViewBackground ());
                    this.menu_search_view.setSearchableInfo (sm.getSearchableInfo (getComponentName ()));
                    
                    if (this.is_searching) {
                    
                        item.expandActionView ();
                        this.is_searching = false;
                    
                    }
                    
                    if (!this.search_query.equals ("")) {
                    
                        this.menu_search_view.setQuery (this.search_query, false);
                        this.search_query = "";
                    
                    }
                
                }
            
            } else {
            
                if (item.getItemId () == R.id.menu_dark_mode) {
                    item.setChecked (this.dark_mode);
                } else if (item.getItemId () == R.id.menu_view_type) {
                
                    switch (this.view_type) {
                    
                        case "grid":
                            item.setIcon (R.drawable.ic_menu_expand);
                            item.setTitle (R.string.menu_view_type_expand);
                            break;
                        
                        case "expand":
                            item.setIcon (R.drawable.ic_menu_list);
                            item.setTitle (R.string.menu_view_type_list);
                            break;
                        
                        case "list":
                            item.setIcon (R.drawable.ic_menu_grid);
                            item.setTitle (R.string.menu_view_type_grid);
                            break;
                    
                    }
                
                }
            
            }
            
            icon = item.getIcon ();
            
            if (icon == null) {
                continue;
            }
            
            icon.setTintList (colorControlNormal);
        
        }
        
        return true;
    
    }
    
    @Override
    protected void onNewIntent (Intent intent) {
    
        if (Intent.ACTION_SEARCH.equals (intent.getAction ())) {
            this.menu_search_view.setQuery (intent.getStringExtra (SearchManager.QUERY), false);
        }
    
    }
    
    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
    
        if (item.getItemId () == R.id.menu_about) {
        
            Intent intent = new Intent (this, AboutActivity.class);
            startActivity (intent);
        
        } else if (item.getItemId () == R.id.menu_dark_mode) {
        
            this.dark_mode = !this.dark_mode;
            saveSettings ();
            
            recreate ();
            return true;
        
        } else if (item.getItemId () == R.id.menu_share && this.action_bar != null) {
        
            this.action_bar_custom_showing = !this.action_bar_custom_showing;
            
            this.action_bar.setDisplayShowCustomEnabled (this.action_bar_custom_showing);
            this.action_bar.setDisplayShowHomeEnabled (!this.action_bar_custom_showing);
            this.action_bar.setDisplayShowTitleEnabled (!this.action_bar_custom_showing);
            
            invalidateOptionsMenu ();
            return true;
        
        } else if (item.getItemId () == R.id.menu_sort) {
        
            Bundle args = new Bundle ();
            args.putBoolean ("DESCENDING", this.descending);
            args.putString ("SORT_BY", this.sort_by);
            
            this.sort_dialog_fragment.setArguments (args);
            this.sort_dialog_fragment.show (getSupportFragmentManager (), "sort_dialog_fragment");
            
            return true;
        
        } else if (item.getItemId () == R.id.menu_view_type) {
        
            switch (this.view_type) {
            
                case "grid":
                    this.view_type = "expand";
                    break;
                
                case "expand":
                    this.view_type = "list";
                    break;
                
                case "list":
                    this.view_type = "grid";
                    break;
            
            }
            
            invalidateOptionsMenu ();
            saveSettings ();
            
            return true;
        
        }
        
        return false;
    
    }
    
    @Override
    protected void onPause () {
        super.onPause ();
        
        if (this.sort_dialog_fragment.isVisible ()) {
            this.show_dialog_fragment_sort = true;
        }
    
    }
    
    @Override
    protected void onResume () {
        super.onResume ();
        
        if (this.show_dialog_fragment_sort) {
        
            this.sort_dialog_fragment.show (getSupportFragmentManager (), "sort_dialog_fragment");
            this.show_dialog_fragment_sort = false;
        
        }
    
    }
    
    @Override
    protected void onSaveInstanceState (Bundle outState) {
        super.onSaveInstanceState (outState);
        
        if (this.menu_search_view != null) {
        
            boolean visible = !this.menu_search_view.isIconified ();
            CharSequence query = this.menu_search_view.getQuery ();
            
            if (query == null) {
                query = "";
            }
            
            outState.putInt ("STATE_SEARCHING", (visible ? 1 : 0));
            outState.putString ("STATE_QUERY", query.toString ());
        
        }
        
        outState.putInt ("STATE_CUSTOM_SHOWING", (this.action_bar_custom_showing ? 1 : 0));
    
    }
    
    @Override
    public void onSortCancelled (String sort_by, boolean descending) {
        android.util.Log.i ("onSortCancelled", sort_by + ", " + descending);
    }
    
    @Override
    public void onSortDone (String sort_by, boolean descending) {
    
        this.descending = descending;
        this.sort_by = sort_by;
        
        saveSettings ();
    
    }
    
    private void saveSettings () {
    
        if (this.prefs == null) {
            this.prefs = getSharedPreferences (getPackageName (), 0);
        }
        
        SharedPreferences.Editor editor = this.prefs.edit ();
        
        editor.putBoolean ("DARK_MODE", this.dark_mode);
        editor.putBoolean ("DESCENDING", this.descending);
        
        editor.putString ("SORT_BY", this.sort_by);
        editor.putString ("VIEW_TYPE", this.view_type);
        
        editor.apply ();
    
    }

}