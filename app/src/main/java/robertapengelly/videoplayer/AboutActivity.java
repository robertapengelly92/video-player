package robertapengelly.videoplayer;

import  android.content.Intent;
import  android.content.SharedPreferences;
import  android.content.pm.PackageInfo;
import  android.content.pm.PackageManager;
import  android.net.Uri;
import  android.os.Bundle;
import  android.provider.Settings;
import  android.support.v4.app.NavUtils;
import  android.support.v7.app.AppCompatActivity;
import  android.support.v7.widget.Toolbar;
import  android.text.Html;
import  android.view.MenuItem;
import  android.view.View;
import  android.widget.TextView;

public class AboutActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean dark_mode = false;
    private SharedPreferences prefs;
    
    private void loadSettings () {
    
        if (this.prefs == null) {
            this.prefs = getSharedPreferences (getPackageName (), 0);
        }
        
        dark_mode = this.prefs.getBoolean ("DARK_MODE", false);
    
    }
    
    @Override
    public void onClick (View view) {
    
        if (view.getId () == R.id.btn_app_info) {
        
            Intent intent = new Intent ();
            intent.setAction (Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.setData (Uri.parse ("package:" + getPackageName ()));
            startActivity (intent);
        
        } else if (view.getId () == R.id.btn_disclaimer) {
        
            Intent intent = new Intent (this, DisclaimerActivity.class);
            startActivity (intent);
        
        }
    
    }
    
    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        
        loadSettings ();
        
        if (this.dark_mode) {
            setTheme (R.style.AppTheme);
        } else {
            setTheme (R.style.AppTheme_Light);
        }
        
        setContentView (R.layout.activity_about);
        
        Toolbar toolbar = findViewById (R.id.action_bar);
        setSupportActionBar (toolbar);
        
        if (getSupportActionBar () != null) {
            getSupportActionBar ().setDisplayHomeAsUpEnabled (true);
        }
        
        findViewById (R.id.btn_app_info).setOnClickListener (this);
        
        TextView version = findViewById (R.id.version);
        
        try {
        
            PackageInfo pInfo = getPackageManager ().getPackageInfo (getPackageName (), 0);
            version.setText (version.getText ().toString ().concat (" ").concat (pInfo.versionName));
        
        } catch (PackageManager.NameNotFoundException e) {
            version.setVisibility (View.GONE);
        }
        
        TextView btn_disclaimer = findViewById (R.id.btn_disclaimer);
        btn_disclaimer.setOnClickListener (this);
        btn_disclaimer.setText (Html.fromHtml (btn_disclaimer.getText ().toString ()));
    
    }
    
    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
    
        switch (item.getItemId()) {
        
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask (this);
                return true;
            default:
                return super.onOptionsItemSelected (item);
        
        }
    
    }

}