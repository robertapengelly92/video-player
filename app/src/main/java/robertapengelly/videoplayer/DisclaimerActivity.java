package robertapengelly.videoplayer;

import  android.content.SharedPreferences;
import  android.os.Bundle;
import  android.support.v4.app.NavUtils;
import  android.support.v7.app.AppCompatActivity;
import  android.support.v7.widget.Toolbar;
import  android.text.Html;
import  android.view.MenuItem;
import  android.widget.TextView;

import  java.io.InputStream;

public class DisclaimerActivity extends AppCompatActivity {

    private boolean dark_mode = false;
    private SharedPreferences prefs;
    
    private void loadSettings () {
    
        if (this.prefs == null) {
            this.prefs = getSharedPreferences (getPackageName (), 0);
        }
        
        dark_mode = this.prefs.getBoolean ("DARK_MODE", false);
    
    }
    
    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        
        loadSettings ();
        
        if (this.dark_mode) {
            setTheme (R.style.AppTheme);
        } else {
            setTheme (R.style.AppTheme_Light);
        }
        
        setContentView (R.layout.activity_disclaimer);
        
        Toolbar toolbar = findViewById (R.id.action_bar);
        setSupportActionBar (toolbar);
        
        if (getSupportActionBar () != null) {
            getSupportActionBar ().setDisplayHomeAsUpEnabled (true);
        }
        
        TextView txt_disclaimer = findViewById (R.id.txt_disclaimer);
        
        try {
        
            InputStream is = getResources ().openRawResource (R.raw.disclaimer);
            byte[] buffer = new byte[is.available ()];
    
            //noinspection StatementWithEmptyBody
            while (is.read (buffer) != -1);
            
            txt_disclaimer.setText (Html.fromHtml (new String (buffer)));
        
        } catch (Exception e) {
            e.printStackTrace ();
        }
    
    }
    
    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
    
        switch (item.getItemId()) {
        
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask (this);
                return true;
            default:
                return super.onOptionsItemSelected (item);
        
        }
    
    }

}