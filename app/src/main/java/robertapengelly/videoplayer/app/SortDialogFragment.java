package robertapengelly.videoplayer.app;

import  android.app.Dialog;
import  android.content.DialogInterface;
import  android.content.res.ColorStateList;
import  android.content.res.TypedArray;
import  android.graphics.drawable.Drawable;
import  android.graphics.drawable.GradientDrawable;
import  android.graphics.drawable.InsetDrawable;
import  android.os.Bundle;
import  android.support.annotation.NonNull;
import  android.support.v7.app.AlertDialog;
import  android.support.v7.app.AppCompatDialogFragment;
import  android.view.LayoutInflater;
import  android.view.View;
import  android.view.ViewGroup;
import  android.view.Window;
import  android.widget.CompoundButton;
import  android.widget.RadioButton;
import  android.widget.RadioGroup;

import  java.util.ArrayList;

import  robertapengelly.videoplayer.R;

public class SortDialogFragment extends AppCompatDialogFragment implements
    DialogInterface.OnClickListener,
    RadioButton.OnCheckedChangeListener {
    
    private boolean descending = true;
    private boolean run_dismiss = true;
    
    private ArrayList<OnDismissListener> listeners;
    private String sort_by = "date_modified";
    
    public void addOnDismissListener (OnDismissListener listener) {
    
        if (this.listeners == null) {
            this.listeners = new ArrayList <> ();
        }
        
        this.listeners.add (listener);
    
    }
    
    private Drawable findCornerDrawable (Drawable drawable) {
    
        if (drawable instanceof GradientDrawable) {
            return drawable;
        }
        
        if (drawable instanceof InsetDrawable) {
            return findCornerDrawable (((InsetDrawable) drawable).getDrawable ());
        }
        
        return drawable;
    
    }
    
    @Override
    public void onCheckedChanged (CompoundButton cb, boolean isChecked) {
    
        if (cb.getId () == R.id.order_descending) {
            this.descending = isChecked;
        } else if (cb.getId () == R.id.order_ascending) {
            this.descending = !isChecked;
        } else if (isChecked) {
            this.sort_by = getResources ().getResourceEntryName (cb.getId ()).substring (8);
        }
    
    }
    
    @Override
    public void onClick (DialogInterface dialog, int which) {
    
        this.run_dismiss = false;
        
        if (this.listeners == null) {
            return;
        }
        
        for (OnDismissListener listener : this.listeners) {
        
            if (which == DialogInterface.BUTTON_NEGATIVE) {
                listener.onSortCancelled (this.sort_by, this.descending);
            } else if (which == DialogInterface.BUTTON_POSITIVE) {
                listener.onSortDone (this.sort_by, this.descending);
            }
        
        }
    
    }
    
    @NonNull
    @Override
    public Dialog onCreateDialog (Bundle savedInstanceState) {
    
        if (getActivity () == null) {
            return super.onCreateDialog (savedInstanceState);
        }
        
        AlertDialog.Builder builder = new AlertDialog.Builder (getActivity ());
        
        builder.setNegativeButton (R.string.dialog_fragment_cancel, this);
        builder.setPositiveButton (R.string.dialog_fragment_done, this);
        
        boolean descending = true;
        int sort_by = -1;
        
        if (getArguments () != null) {
        
            Bundle args = getArguments ();
            
            descending = args.getBoolean ("DESCENDING", false);
            sort_by = getResources ().getIdentifier ("sort_by_".concat (args.getString ("SORT_BY", "")), "id", getActivity ().getPackageName ());
        
        }
        
        ViewGroup content = getActivity ().findViewById (android.R.id.content);
        View view = LayoutInflater.from (getActivity ()).inflate (R.layout.dialog_fragment_sort, content, false);
        
        ColorStateList colorControlNormal;
        TypedArray ta = getActivity ().getTheme ().obtainStyledAttributes (new int[] { R.attr.colorControlNormal });
        
        try {
            colorControlNormal = ta.getColorStateList (0);
        } finally {
            ta.recycle ();
        }
        
        GradientDrawable gd = new GradientDrawable ();
        gd.setColor (colorControlNormal);
        
        view.findViewById (R.id.separator).setBackground (gd);
        
        RadioGroup group = view.findViewById (R.id.group_sort_by);
        
        int count = group.getChildCount ();
        RadioButton button;
        
        for (int i = 0; i < count; i++) {
        
            button = (RadioButton) group.getChildAt (i);
            button.setOnCheckedChangeListener (this);
            
            if (button.getId () == sort_by) {
                group.check (button.getId ());
            }
        
        }
        
        group = view.findViewById (R.id.group_order);
        
        if (descending) {
            group.check (group.findViewById (R.id.order_descending).getId ());
        } else {
            group.check (group.findViewById (R.id.order_ascending).getId ());
        }
        
        count = group.getChildCount ();
        
        for (int i = 0; i < count; i++) {
            ((RadioButton) group.getChildAt (i)).setOnCheckedChangeListener (this);
        }
        
        builder.setView (view);
        
        Dialog dialog = builder.create ();
        
        if (dialog.getWindow () != null) {
        
            Window w = dialog.getWindow ();
            Drawable d = findCornerDrawable (w.getDecorView ().getBackground ());
            
            if (d instanceof GradientDrawable) {
            
                int radii = getResources ().getDimensionPixelSize (R.dimen.dialog_fragment_radii);
                ((GradientDrawable) d).setCornerRadii (new float[] { radii, radii, radii, radii, radii, radii, radii, radii });
            
            }
        
        }
        
        return dialog;
    
    }
    
    @Override
    public void onDismiss (DialogInterface dialog) {
        super.onDismiss (dialog);
        
        if (this.listeners == null) {
            return;
        }
        
        if (this.run_dismiss) {
        
            for (OnDismissListener listener : this.listeners) {
                listener.onSortCancelled (sort_by, descending);
            }
        
        }
        
        // Reset our boolean ready for the next time we're open.
        this.run_dismiss = true;
    
    }
    
    public interface OnDismissListener {
        void onSortCancelled (String sort_by, boolean descending);
        void onSortDone (String sort_by, boolean descending);
    }

}